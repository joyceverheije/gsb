<?php

use PHPUnit\Framework\TestCase;

include('../include/fct.inc.php');

class FunctionsTests extends TestCase
{
    public function testHelpersFunctions()
    {
        $this->assertEquals('201706', getMois('10/06/2017'));
        $this->assertEquals('16/06/2017', dateAnglaisVersFrancais('2017-06-16'));
        $this->assertEquals('04/04/1504', dateAnglaisVersFrancais('1504-04-04'));

        $this->assertTrue(estTableauEntiers([1, 2, 3]));
        $this->assertFalse(estTableauEntiers([1, 'deux', 3]));

        $this->assertTrue(estDateDepassee('04/04/1504'));
        $this->assertFalse(estDateDepassee('04/04/2017'));
    }

    public function testAuthentification()
    {
        $this->assertFalse(estConnecte());
        $_SESSION['idVisiteur'] = 1;
        $this->assertTrue(estConnecte());
        $_SESSION['type'] = 'Comptable';
        $this->assertTrue(estComptable());
        $this->assertFalse(estVisiteur());
        $_SESSION['idVisiteur'] = null;
        $this->assertFalse(estConnecte());
    }
}
