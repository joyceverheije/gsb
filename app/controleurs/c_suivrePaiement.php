<?php

$action = $_REQUEST['action'];
$lesVisiteurs = $pdo->getLesVisiteursEtat(['VA', 'MP']);

include("vues/v_sommaire.php");
include("vues/suivrePaiement/v_suivrePaiement.php");

if (empty($lesVisiteurs)) {
	ajouterErreur("Pas de fiche de frais à suivre");
	include("vues/v_erreurs.php");
} elseif ($action === 'selectionnerVisiteur') {
	include("vues/suivrePaiement/v_choixVisiteur.php");
} else {
	$visiteurASelectionner = $_REQUEST['lstVisiteur'] ?? null;
	$moisASelectionner = $_REQUEST['lstMois'] ?? null; 
	$lesMois = $pdo->getLesMoisDisponiblesEtat($visiteurASelectionner, ['VA', 'MP']);

	if (empty($lesMois)) {
		ajouterErreur("Pas de fiche de frais pour ce visiteur");
		$visiteurASelectionner = null;
		include("vues/suivrePaiement/v_choixVisiteur.php");
		include("vues/v_erreurs.php");
	} else {
		switch($action){
			case 'selectionnerMois':{
				$lesCles = array_keys($lesMois);
				$moisASelectionner = $lesCles[0];

				include("vues/suivrePaiement/v_choixMois.php");
				break;
			}
			case 'voirFraisVisiteurMois':{
				list($lesInfosFicheFrais, $lesFraisForfait, $lesFraisHorsForfait) = $pdo->getInfosFicheFrais($visiteurASelectionner, $moisASelectionner);

				include("vues/suivrePaiement/v_choixMois.php");

				if (empty($lesInfosFicheFrais)) {
					ajouterErreur("Pas de fiche de frais pour ce visiteur ce mois");
					include("vues/v_erreurs.php");
				} else {
					list($libEtat, $montantValide, $nbJustificatifs, $dateModif, $numAnnee, $numMois) = $pdo->getDetailsFicheFrais($lesInfosFicheFrais, $moisASelectionner);
					
					include("vues/etatFrais/v_etatFrais.php");
					include("vues/suivrePaiement/v_actions.php");
				}

				break;
			}
			case 'mettreEnPaiement':{
				$pdo->majEtatFicheFrais($visiteurASelectionner,$moisASelectionner,'MP');
				
				list($lesInfosFicheFrais, $lesFraisForfait, $lesFraisHorsForfait) = $pdo->getInfosFicheFrais($visiteurASelectionner, $moisASelectionner);

				list($libEtat, $montantValide, $nbJustificatifs, $dateModif, $numAnnee, $numMois) = $pdo->getDetailsFicheFrais($lesInfosFicheFrais, $moisASelectionner);
				
				$messageSucces = 'Mise en paiement de la fiche prise en compte';

				include("vues/suivrePaiement/v_choixMois.php");
				include("vues/etatFrais/v_etatFrais.php");
				include("vues/suivrePaiement/v_actions.php");

				break;
			}
			case 'confirmerPaiement':{
				$pdo->majEtatFicheFrais($visiteurASelectionner,$moisASelectionner,'RB');
				
				list($lesInfosFicheFrais, $lesFraisForfait, $lesFraisHorsForfait) = $pdo->getInfosFicheFrais($visiteurASelectionner, $moisASelectionner);
				
				list($libEtat, $montantValide, $nbJustificatifs, $dateModif, $numAnnee, $numMois) = $pdo->getDetailsFicheFrais($lesInfosFicheFrais, $moisASelectionner);
				
				$messageSucces = 'Remboursement de la fiche pris en compte';

				include("vues/suivrePaiement/v_choixMois.php");
				include("vues/etatFrais/v_etatFrais.php");
				include("vues/suivrePaiement/v_actions.php");

				break;
			}
		}
	}
}

?>