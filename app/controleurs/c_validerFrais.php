<?php

$mois = getMois(date("d/m/Y"));
$numAnnee = substr( $mois,0,4);
$numMois = substr( $mois,4,2);

$action = $_REQUEST['action'];
$lesVisiteurs = $pdo->getLesVisiteursEtat(['CL']);

include("vues/v_sommaire.php");
include("vues/validerFrais/v_validerFrais.php");

if (empty($lesVisiteurs)) {
	ajouterErreur("Pas de fiche de frais à valider");
	include("vues/v_erreurs.php");
} else if ($action === 'selectionnerVisiteur') {	
	include("vues/validerFrais/v_choixVisiteur.php");
} else {
	$visiteurASelectionner = $_REQUEST['lstVisiteur'] ?? null;
	$moisASelectionner = $_REQUEST['lstMois'] ?? null; 
	$lesMois = $pdo->getLesMoisDisponiblesEtat($visiteurASelectionner, ['CL']);

	if (empty($lesMois)) {
		ajouterErreur("Pas de fiche de frais à valider pour ce visiteur");
		$visiteurASelectionner = null;
		include("vues/validerFrais/v_choixVisiteur.php");
		include("vues/v_erreurs.php");
	} else {
		switch($action){
			case 'selectionnerMois':{
				$lesCles = array_keys($lesMois);
				$moisASelectionner = $lesCles[0];

				include("vues/validerFrais/v_choixMois.php");
				break;
			}
			case 'validerMajFraisForfait':{
				$lesFrais = $_REQUEST['lesFrais'];
				
				if(lesQteFraisValides($lesFrais)){
			  	 	$pdo->majFraisForfait($visiteurASelectionner,$moisASelectionner,$lesFrais);
					$messageSucces = 'Modification des frais forfaitisés prise en compte';
				}
				else{
					ajouterErreur("Les valeurs des frais doivent être numériques");
					include("vues/v_erreurs.php");
				}
			}
			case 'voirFraisVisiteurMois':{
				list($lesInfosFicheFrais, $lesFraisForfait, $lesFraisHorsForfait) = $pdo->getInfosFicheFrais($visiteurASelectionner, $moisASelectionner);

				$moisSuivant = getMoisSuivant();

				include("vues/validerFrais/v_choixMois.php");

				if (empty($lesInfosFicheFrais)) {
					ajouterErreur("Pas de fiche de frais à valider pour ce visiteur ce mois");
					include("vues/v_erreurs.php");
				} else {
					include("vues/validerFrais/v_listeFrais.php");
				}

				break;
			}
			case 'refuserFrais':{
				$pdo->refuseFraisHorsForfait($_REQUEST['idFrais']);

				$moisSuivant = getMoisSuivant();
				list($lesInfosFicheFrais, $lesFraisForfait, $lesFraisHorsForfait) = $pdo->getInfosFicheFrais($visiteurASelectionner, $moisASelectionner);

				include("vues/validerFrais/v_choixMois.php");
				include("vues/validerFrais/v_listeFrais.php");

				break;
			}
			case 'reporterFrais':{
				$moisSuivant = getMoisSuivant();

				if ($pdo->estPremierFraisMois($visiteurASelectionner, $moisSuivant)) {
					$pdo->creeNouvellesLignesFrais($visiteurASelectionner, $moisSuivant);
				}

				$pdo->reporteFraisHorsForfait($_REQUEST['idFrais'], $moisSuivant);

				list($lesInfosFicheFrais, $lesFraisForfait, $lesFraisHorsForfait) = $pdo->getInfosFicheFrais($visiteurASelectionner, $moisASelectionner);

				include("vues/validerFrais/v_choixMois.php");
				include("vues/validerFrais/v_listeFrais.php");

				break;
			}
			case 'validerFiche':{
				$pdo->majEtatFicheFrais($visiteurASelectionner,$moisASelectionner,'VA');
				
				$moisSuivant = getMoisSuivant();
				list($lesInfosFicheFrais, $lesFraisForfait, $lesFraisHorsForfait) = $pdo->getInfosFicheFrais($visiteurASelectionner, $moisASelectionner);

				$messageSucces = 'Validation de la fiche prise en compte';
				
				list($libEtat, $montantValide, $nbJustificatifs, $dateModif, $numAnnee, $numMois) = $pdo->getDetailsFicheFrais($lesInfosFicheFrais, $moisASelectionner);

				$valide = true;

				include("vues/validerFrais/v_choixMois.php");
				include("vues/etatFrais/v_etatFrais.php");

				break;
			}
		}
	}
}

?>