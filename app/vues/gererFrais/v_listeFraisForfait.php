
<h2 class="title">Renseigner ma fiche de frais du mois <?php echo $numMois."-".$numAnnee ?></h2><hr>
 
<form method="POST" action="index.php?uc=gererFrais&action=validerMajFraisForfait">
	<div class="control">
		<h2 class="subtitle">Eléments forfaitisés</h2>
		<div class="columns is-multiline">
				<?php
				foreach ($lesFraisForfait as $unFrais)
				{
					$idFrais = $unFrais['idfrais'];
					$libelle = $unFrais['libelle'];
					$quantite = $unFrais['quantite'];
				?>
			<div class="column is-half">
				<div class="field">
					<label class="label" for="idFrais"><?php echo $libelle ?></label>
					<p class="control">
				    	<input class="input" id="idFrais" name="lesFrais[<?php echo $idFrais?>]" type="text" maxlength="5" value="<?php echo $quantite?>">
				  	</p>
				</div>
			</div>
				<?php
				}
				?>
		</div>
	    <input class="button is-primary" id="ajouter" type="submit" value="Ajouter">
    	<input class="button is-danger" id="effacer" type="reset" value="Effacer">
	</div>
	<hr>
</form>
