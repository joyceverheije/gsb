<h2 class="subtitle">Descriptif des éléments hors forfait</h2>

<table class="table">
  <thead>
    <tr>
      <th>Date</th>
      <th>Libellé</th>  
      <th>Montant</th>  
      <th></th>   
    </tr>
  </thead>
  <tbody>
    <?php    
    foreach($lesFraisHorsForfait as $unFraisHorsForfait) 
    {
      $libelle = $unFraisHorsForfait['libelle'];
      $date = $unFraisHorsForfait['date'];
      $montant=$unFraisHorsForfait['montant'];
      $id = $unFraisHorsForfait['id'];
    ?>    
    <tr>
      <td><?php echo $date ?></td>
      <td><?php echo $libelle ?></td>
      <td><?php echo $montant ?></td>
      <td><a href="index.php?uc=gererFrais&action=supprimerFrais&idFrais=<?php echo $id ?>" 
            onclick="return confirm('Voulez-vous vraiment supprimer ce frais?');">Supprimer ce frais</a></td>
    </tr>
    <?php                  
    }
    ?>    
  </tbody>
</table>
<hr>

<h2 class="subtitle">Nouvel élément hors forfait</h2>
<form action="index.php?uc=gererFrais&action=validerCreationFrais" method="post">
  <div class="control">
    <div class="field">
      <label class="label">Date (jj/mm/aaaa)</label>
      <p class="control">
        <input class="input" name="dateFrais" type="text">
      </p>
    </div>
    <div class="field">
      <label class="label">Libellé</label>
      <p class="control">
        <input class="input" name="libelle" maxlength="100" type="text">
      </p>
    </div>
    <div class="field">
      <label class="label">Montant</label>
      <p class="control">
        <input class="input" name="montant" type="text">
      </p>
    </div>
    <p>
      <input class="button is-primary" id="ajouter" type="submit" value="Ajouter" size="20" />
      <input class="button is-danger" id="effacer" type="reset" value="Effacer" size="20" />
    </p> 
  </div>
</form>

  

