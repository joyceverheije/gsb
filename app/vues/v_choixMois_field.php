<div>
    <h3 class="subtitle">Sélectionner un mois: </h3>
    <div class="field">
        <p class="control">
            <span class="select">
                <select class="select" id="lstMois" name="lstMois">
                <?php
                    foreach ($lesMois as $unMois)
                    {
                        $mois = $unMois['mois'];
                        $numAnnee =  $unMois['numAnnee'];
                        $numMois =  $unMois['numMois'];
                        if($mois == $moisASelectionner){
                        ?>
                        <option selected value="<?php echo $mois ?>"><?php echo  $numMois."/".$numAnnee ?> </option>
                        <?php 
                        }
                        else{ ?>
                        <option value="<?php echo $mois ?>"><?php echo  $numMois."/".$numAnnee ?> </option>
                        <?php 
                        }
                    }
                ?>    
                </select>
            </span>
        </p>
    </div>
    <input class="button is-primary" id="ok" type="submit" value="Rechercher" size="20" />
    <hr>
</div>