<!-- Sommaire -->

<div class="columns">
  <div class="column is-one-quarter">
    <aside class="menu box">
      <p class="menu-label">
        Utilisateur
      </p>
      <ul class="menu-list">
        <li><?php echo $_SESSION['prenom'] . "  " . $_SESSION['nom'] . " (" . $_SESSION['type'] . ")" ?></li>
      </ul>
      <p class="menu-label">
        Actions
      </p>
      <ul class="menu-list">
        <?php if (estComptable()): ?>
          <li>
            <a href="index.php?uc=validerFrais&action=selectionnerVisiteur" title="Valider fiche de frais">Valider fiche de frais</a>
          </li>
          <li>
            <a href="index.php?uc=suivrePaiement&action=selectionnerVisiteur" title="Suivre paiement fiche de frais">Suivre paiement<br>fiche de frais</a>
          </li>
        <?php else: ?>
          <li>
            <a href="index.php?uc=gererFrais&action=saisirFrais" title="Saisie fiche de frais">Saisie fiche de frais</a>
          </li>
          <li>
            <a href="index.php?uc=etatFrais&action=selectionnerMois" title="Consultation de mes fiches de frais">Mes fiches de frais</a>
          </li>
        <?php endif; ?>
          <li><a href="index.php?uc=connexion&action=deconnexion" title="Se déconnecter">Déconnexion</a></li>
      </ul>
    </aside>
  </div>
  <div class="column">