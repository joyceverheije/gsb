<div>
    <h3 class="subtitle">Sélectionner un visiteur: </h3>
    <div class="field">
        <p class="control">
            <span class="select">
                <select class="select" id="lstVisiteur" name="lstVisiteur">
                    <?php
                    foreach ($lesVisiteurs as $unVisiteur)
                    {
                        $id =  $unVisiteur['id'];
                        $prenom = $unVisiteur['prenom'];
                        $nom =  $unVisiteur['nom'];
                        $nomComplet = $prenom . " " . $nom;
                        if($id == $visiteurASelectionner){
                        ?>
                        <option selected value="<?php echo $id ?>"><?php echo $nomComplet ?> </option>
                        <?php 
                        } else { ?>
                        <option value="<?php echo $id ?>"><?php echo $nomComplet ?> </option>
                        <?php 
                        }
                    }
                    ?>
                </select>
            </span>
        </p>
    </div>
    <?php 
        if (!isset($visiteurASelectionner)) {
    ?>
    <input class="button is-primary" id="ok" type="submit" value="Rechercher" size="20" />
    <hr>
    <?php 
        }
    ?>  
    <br>
</div>