<?php if($lesInfosFicheFrais['idEtat'] === 'VA'): ?>
	<a class="button is-primary" href="index.php?uc=suivrePaiement&action=mettreEnPaiement&lstMois=<?php echo $moisASelectionner ?>&lstVisiteur=<?php echo $visiteurASelectionner ?>" onclick="return confirm('Voulez-vous vraiment mettre au paiement cette fiche?');">Mettre en paiement</a>
<?php endif; ?>

<?php if(in_array($lesInfosFicheFrais['idEtat'], ['MP', 'VA'])): ?>
<a class="button is-primary" href="index.php?uc=suivrePaiement&action=confirmerPaiement&lstMois=<?php echo $moisASelectionner ?>&lstVisiteur=<?php echo $visiteurASelectionner ?>" onclick="return confirm('Voulez-vous vraiment confirmer le paiement de cette fiche?');">Confirmer paiement</a>
<?php endif; ?>