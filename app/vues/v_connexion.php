<section class="hero is-bold">
  <div class="hero-body">
    <div class="container">
      <div class="columns is-vcentered">
        <div class="column is-4 is-offset-4">
          <h1 class="title">Identification utilisateur</h1>
          <div class="box">
            <form method="POST" action="index.php?uc=connexion&action=valideConnexion">
              <label class="label">Login</label>
              <p class="control required">
                <input name="login" class="input" type="text" placeholder="login">
              </p>
              <label class="label">Mot de passe</label>
              <p class="control required">
                <input name="mdp" class="input" type="password" placeholder="●●●●●●●">
              </p>
              <hr>
              <p class="control">
                <input type="submit" class="button is-primary" value="Valider" name="valider">
                <input type="reset" class="button is-default" value="Annuler" name="annuler">
              </p>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>