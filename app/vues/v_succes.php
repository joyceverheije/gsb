<?php if (isset($messageSucces)): ?>
	<div class="notification is-success">
	  <button class="delete"></button>
	  <p>
	  	<?php 
	  		echo $messageSucces;
	  	?>
	  </p>
	</div>
<?php endif; ?>