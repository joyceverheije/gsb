<?php
	include("vues/v_succes.php"); 
?>
<div class="control box">
	<h2 class="subtitle">Eléments forfaitisés</h2><br>
	<form method="POST" action="index.php?uc=validerFrais&action=validerMajFraisForfait"> 
	  	<input type="hidden" name="lstMois" value="<?php echo $moisASelectionner ?>">
	  	<input type="hidden" name="lstVisiteur" value="<?php echo $visiteurASelectionner ?>">
		
		<div class="columns is-multiline">
				<?php
				foreach ($lesFraisForfait as $unFrais)
				{
					$idFrais = $unFrais['idfrais'];
					$libelle = $unFrais['libelle'];
					$quantite = $unFrais['quantite'];
				?>
			<div class="column is-half">
				<div class="field">
					<label class="label" for="idFrais"><?php echo $libelle ?></label>
					<p class="control">
				    	<input class="input" id="idFrais" name="lesFrais[<?php echo $idFrais?>]" type="text" maxlength="5" value="<?php echo $quantite?>">
				  	</p>
				</div>
			</div>
				<?php
				}
				?>
		</div>
		<input class="button is-primary" id="ajouter" type="submit" value="Modifier">  
	</form>
	<br>
	<h2 class="subtitle">Eléments hors forfait</h2><br>

	<table class="table">
	  <thead>
	    <tr>
	      <th>Date</th>
	      <th>Libellé</th>  
	      <th>Montant</th>  
	      <th></th>   
	      <th></th>   
	    </tr>
	  </thead>
	  <tbody>
	    <?php    
	    foreach($lesFraisHorsForfait as $unFraisHorsForfait) 
	    {
	      $libelle = $unFraisHorsForfait['libelle'];
	      $date = $unFraisHorsForfait['date'];
	      $montant=$unFraisHorsForfait['montant'];
	      $id = $unFraisHorsForfait['id'];
	    ?>    
	    <tr>
	      <td><?php echo $date ?></td>
	      <td><?php echo $libelle ?></td>
	      <td><?php echo $montant ?></td>
	      <?php if (substr($libelle, 0, strlen('REFUSE')) !== 'REFUSE'): ?>
		      <td>
		      	<a href="index.php?uc=validerFrais&action=refuserFrais&idFrais=<?php echo $id ?>&lstMois=<?php echo $moisASelectionner ?>&lstVisiteur=<?php echo $visiteurASelectionner ?>" onclick="return confirm('Voulez-vous vraiment refuser ce frais?');">Refuser ce frais</a>
		      </td>
		  	<?php if ($moisASelectionner !== $moisSuivant): ?>
		      <td>
		      	<a href="index.php?uc=validerFrais&action=reporterFrais&idFrais=<?php echo $id ?>&lstMois=<?php echo $moisASelectionner ?>&lstVisiteur=<?php echo $visiteurASelectionner ?>" onclick="return confirm('Voulez-vous vraiment reporter ce frais?');">Reporter ce frais</a>
		      </td>
		  	<?php else: ?>
		  		<td>
		  	<?php endif; ?>
		  <?php else: ?>
		  	<td></td>
		  	<td></td>
		  <?php endif; ?>
	    </tr>
	    <?php                  
	    }
	    ?>    
	  </tbody>
	</table>
	<?php if(!isset($valide)): ?>
		<a class="button is-primary" href="index.php?uc=validerFrais&action=validerFiche&lstMois=<?php echo $moisASelectionner ?>&lstVisiteur=<?php echo $visiteurASelectionner ?>" onclick="return confirm('Voulez-vous vraiment valider cette fiche de frais?');">Valider la fiche</a>
	<?php endif; ?>
</div>