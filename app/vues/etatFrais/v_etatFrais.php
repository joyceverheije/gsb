<?php include("vues/v_succes.php"); ?>

<div class="box">
  <h3 class="subtitle">Fiche de frais du mois <?php echo $numMois."-".$numAnnee?></h3>
  <p class="subtitle">Etat : <?php echo $libEtat?> depuis le <?php echo $dateModif?></p>
  <p class="subtitle">Montant validé : <?php echo $montantValide?> €</p><br>
  <p class="subtitle">Eléments forfaitisés</p>
  <table class="table">
    <thead>
      <tr>
        <?php
        foreach ( $lesFraisForfait as $unFraisForfait ) 
        {
        $libelle = $unFraisForfait['libelle'];
        ?>  
              <th> <?php echo $libelle?></th>
             <?php
                }
            ?> 
      </tr>
      <tr>
        <?php
          foreach (  $lesFraisForfait as $unFraisForfait  ) 
          {
          $quantite = $unFraisForfait['quantite'];
        ?>
        <td class="qteForfait"><?php echo $quantite?> </td>
        <?php
          }
        ?>
      </tr>
    </thead>
  </table>
  <p class="subtitle">Descriptif des éléments hors forfait (<?php echo $nbJustificatifs ?> justificatifs reçus)</p>
  <table class="table">
    <thead>
      <tr>
         <th class="date">Date</th>
         <th class="libelle">Libellé</th>
         <th class='montant'>Montant</th>                
      </tr>
      <?php      
        foreach ( $lesFraisHorsForfait as $unFraisHorsForfait ) 
        {
        $date = $unFraisHorsForfait['date'];
        $libelle = $unFraisHorsForfait['libelle'];
        $montant = $unFraisHorsForfait['montant'];
      ?>
      <tr>
         <td><?php echo $date ?></td>
         <td><?php echo $libelle ?></td>
         <td><?php echo $montant ?> €</td>
      </tr>
      <?php 
        }
      ?>
    </thead>
  </table>
</div>