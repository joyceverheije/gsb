<h2 class="title">Mes fiches de frais</h2><hr>
<div>
	<form action="index.php?uc=etatFrais&action=voirEtatFrais" method="post">
		<div class="field">
		<label class="subtitle label">Sélectionner un mois: </label>
		  <p class="control">
		    <span class="select">
		      <select id="lstMois" name="lstMois">
		        <?php
				foreach ($lesMois as $unMois)
				{
				    $mois = $unMois['mois'];
					$numAnnee =  $unMois['numAnnee'];
					$numMois =  $unMois['numMois'];
					if($mois == $moisASelectionner){
					?>
					<option selected value="<?php echo $mois ?>"><?php echo  $numMois."/".$numAnnee ?> </option>
					<?php 
					}
					else{ ?>
					<option value="<?php echo $mois ?>"><?php echo  $numMois."/".$numAnnee ?> </option>
					<?php 
					}
				
				}
	           
			   ?>
		      </select>
		    </span>
		  </p>
		</div>
		<div class="field is-grouped">
		  <p class="control">
		    <button class="button is-primary" id="ok" type="submit" value="Valider">Valider</button>
		  </p>
		  <p class="control">
		    <button class="button is-link" id="annuler" type="reset" value="Effacer">Effacer</button>
		  </p>
		</div>
		<hr>
    </form>
</div>