SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `gsb_frais`
--

-- --------------------------------------------------------

DELIMITER $$
CREATE TRIGGER update_fichefrais_validation BEFORE UPDATE
ON fichefrais FOR EACH ROW
BEGIN
	DECLARE montantTotal DOUBLE;
	IF NEW.idEtat = 'VA' -- la fiche est validée par un comptable
		THEN BEGIN 		
  		SELECT SUM(montant) into montantTotal FROM (
  			select sum(quantite*montant) as montant -- somme des frais forfait (quantité multipliée par taux du frais)
			from fraisforfait, lignefraisforfait
			where idVisiteur = NEW.idVisiteur
			and mois = NEW.mois
			and idFraisForfait = id
			group by idVisiteur, mois
		UNION ALL
			select sum(montant) as montant -- somme des frais hors forfait non refusés
			from lignefraishorsforfait
			where idVisiteur = NEW.idVisiteur
			and mois = NEW.mois
			and libelle NOT LIKE 'REFUSE%'
			group by idVisiteur, mois
		)montant;
		
		SET NEW.montantValide = montantTotal;
    	
    	END;
    END IF;
END $$
DELIMITER ;