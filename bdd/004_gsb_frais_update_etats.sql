SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `gsb`
--

-- --------------------------------------------------------

UPDATE `Etat` SET `libelle` = 'Validée' WHERE `id` = 'VA';
INSERT INTO `Etat` (`id`, `libelle`) VALUES ('MP', 'Mise en paiement');