SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `gsb_frais`
--

CREATE TABLE IF NOT EXISTS `fraiskilometrique` (
  `id` char(3) NOT NULL,
  `libelle` char(30) DEFAULT NULL,
  `montant` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


INSERT INTO `fraiskilometrique` (`id`, `libelle`, `montant`)
VALUES
	('4D','Véhicule 4CV Diesel',0.52),
	('4E','Véhicule 4CV Essence',0.62),
	('56D','Véhicule 5/6CV Diesel',0.58),
	('56E','Véhicule 5/6CV Essence',0.67);

ALTER TABLE `visiteur` 
ADD `idFraisKilometrique` CHAR(3) NOT NULL;

UPDATE visiteur SET `idFraisKilometrique` = '4E';

ALTER TABLE `visiteur` 
ADD CONSTRAINT `visiteur_fk_fraiskilometrique` 
FOREIGN KEY (`idFraisKilometrique`) 
REFERENCES `fraiskilometrique` (`id`);

LOCK TABLES fichefrais WRITE;
DROP TRIGGER update_fichefrais_validation; 
DELIMITER $$
CREATE TRIGGER update_fichefrais_validation BEFORE UPDATE
ON fichefrais FOR EACH ROW
BEGIN
	DECLARE montantTotal DOUBLE;
	IF NEW.idEtat = 'VA' -- la fiche est validée par un comptable
		THEN BEGIN 		
  		SELECT SUM(montant) into montantTotal FROM (
  			select quantite*montant as montant  -- calcul des frais kilometrique en fonction de la puissance du véhicule du visiteur
  			from lignefraisforfait lff, visiteur v, fraiskilometrique fk
  			where lff.idVisiteur = NEW.idVisiteur
  			and lff.idFraisForfait = 'KM'
  			and lff.mois = NEW.mois
  			and v.id = NEW.idVisiteur
  			and v.idFraisKilometrique = fk.id
  		UNION ALL
  			select sum(quantite*montant) as montant -- somme des frais forfait (quantité multipliée par taux du frais), excluant les frais kilometriques
			from fraisforfait, lignefraisforfait
			where idVisiteur = NEW.idVisiteur
			and mois = NEW.mois
			and idFraisForfait = id
			and idFraisForfait <> 'KM'
			group by idVisiteur, mois
		UNION ALL
			select sum(montant) as montant -- somme des frais hors forfait non refusés
			from lignefraishorsforfait
			where idVisiteur = NEW.idVisiteur
			and mois = NEW.mois
			and libelle NOT LIKE 'REFUSE%'
			group by idVisiteur, mois
		)montant;
		
		SET NEW.montantValide = montantTotal;
    	
    	END;
    END IF;
END $$
DELIMITER ;
UNLOCK TABLES;


